use std::{fs};
use std::path::Path;

extern crate exif;
extern crate chrono;

pub mod structs;
pub mod cmd_line_arguments;
pub mod embedded_data;

fn get_string(input: &str, length: usize){
    let mut a = str::chars(input);
    let mut b: Vec<char> = vec!();
    for i in 0..length{
       
    }    
}

fn sub_string(data: &str, start: usize, len: usize) -> String {
    String::from(data).chars().skip(start).take(len).collect()
}

use std::iter::FromIterator;

fn main() {
    let result = cmd_line_arguments::get_arguments();
    if !result.is_ok() {
        println!("-h for help");
        return;
    }

    let arguments = result.unwrap();

    //Get start path
    let start_path_option = &cmd_line_arguments::get_argument_value("-i", &arguments);   
    let start_path = match start_path_option {
        Some(value) => value.to_owned(),
        None => {
            println!("-h for help");
            return;
        },
    };

    //Running in quiet_mode
    let quiet_mode = &cmd_line_arguments::argument_exists("-q", &arguments);   

    //Assume Yes to overwrite questions
    let overwrite_mode = &cmd_line_arguments::argument_exists("-y", &arguments);
    
    let mut tree_analysis = structs::TreeAnalysis {
        size_to_delete: 0_u64,
        total_directories: 0_u64,
        total_files: 0_u64,
        total_duplicate_files: 0_u64,
    };

    let mut directory = structs::Directory {
        name: start_path.to_owned(),
        files: vec!(),
        directories: vec!(),
    };

    let ym_argument = match cmd_line_arguments::get_argument_value("-ym", &arguments)
    {
        Some(value) => value,
        None => String::new()
    };
    

    println!("Start traversing directories...");
    //First traverse the list of given directories    
    if cmd_line_arguments::argument_exists("-d", &arguments) {
        println!("Scanning supplied list of directories first");
        let argument_value = str::replace(&cmd_line_arguments::get_argument_value("-d", &arguments).unwrap(),"\"","");
        let directories_to_scan_firstx: Vec<String> = argument_value.split(",").map(|s| s.to_string()).collect(); 
        let directories_to_scan_first = directories_to_scan_firstx.clone();    
        let mut index: usize = 0;

        let mut previous_index = 0;

        for current_index in 0..directories_to_scan_first.len() {            
            //let current_scan_directory = get_string(&directories_to_scan_first[index], directories_to_scan_first[index].len());            
            //if current_index == index {
            //    println!("Dir: {}, len: {}", directories_to_scan_first[current_index], directories_to_scan_first[current_index].len());                                       
            //}
            
            let mut dir = "".to_string();
            dir = directories_to_scan_firstx[current_index].clone();


            //let current_scan_directory2 = String::from_utf8_unchecked(current_scan_directory);
            //println!("Directory to scan: {}, Len:{}", current_scan_directory, directories_to_scan_first[index].len());                  
            //let sub_string_len = current_scan_directory.len();            
            //let slice = &current_scan_directory.chars().take(sub_string_len).collect::<String>();            
            //let ll:usize = sub_string_len;
            //let slice = &slice.chars().skip(0).take(ll).collect::<String>();
            //let slice2 = format!("{}", current_scan_directory);

            
            //key.chars().take(key_length).collect()
            traverse_directories(&quiet_mode, &arguments, &start_path, &dir, &mut directory, &mut tree_analysis, &ym_argument, overwrite_mode, &Some(dir.clone()));
            index = index + 1;            
        }
        println!("Done supplied directories! Moving on to other directories");
        println!("");
        println!("");
    }
    
    traverse_directories(&quiet_mode, &arguments, &start_path, &start_path, &mut directory, &mut tree_analysis, &ym_argument, overwrite_mode, &None);
    println!("");
    println!("");
    println!("Done!");
    println!("Scanned {} directories and found a total of {} file(s).", tree_analysis.total_directories, tree_analysis.total_files);
    if tree_analysis.total_duplicate_files > 0 {
        let mut disk_space: String = String::from(format!("{}KB", round_float((tree_analysis.size_to_delete as f64)/1024.0)));
        if round_float((tree_analysis.size_to_delete as f64)/1024.0/1024.0) >= 750.0 {
            disk_space = String::from(format!("{}GB", round_float((tree_analysis.size_to_delete as f64)/1024.0/1024.0/1024.0)))
        } else if round_float((tree_analysis.size_to_delete as f64)/1024.0) >= 750.0 {
            disk_space = String::from(format!("{}MB", round_float((tree_analysis.size_to_delete as f64)/1024.0/1024.0/1024.0)))
        }        
        println!("{} used by {} duplicate file(s)", disk_space, tree_analysis.total_duplicate_files);
    } else {
        println!("No duplicate files were found");
    }
}

fn traverse_directories(quiet_mode: &bool, arguments: &Vec<cmd_line_arguments::Argument>, start_root_directory: &str, start_directory: &str, mut current_directory: &mut structs::Directory, mut tree_analysis: &mut structs::TreeAnalysis, ym_argument: &str, overwrite_mode: &bool, do_only_this_directory: &Option<String>) {
    tree_analysis.total_directories += 1;
    let mut dont_do_anything_to_files: bool = false;

    let result_directory_contents = fs::read_dir(start_directory);
    if result_directory_contents.is_ok() {
        println!("In start_directory: {}", start_directory);
        for entry in fs::read_dir(start_directory).unwrap() {
            let entry = entry.unwrap();
            let path = entry.path();
            let metadata = fs::metadata(&path).unwrap();

            let entry_name = path.display().to_string();

            //println!("Current dir: {}", entry_name);
            if do_only_this_directory.is_some()
            {
                let directory_name_stripped = str::replace(&do_only_this_directory.clone().unwrap(), "\"", "");
                if entry_name.starts_with(&directory_name_stripped)== false
                {
                    //println!("Name does not start with: {}, it is: {}", directory_name_stripped, entry_name);
                    //dont_do_anything_to_files = true;
                    break;
                } else {
                    //println!("Doing: {}", entry_name);
                }
            }

            let result_path = path.file_name().unwrap().to_os_string().into_string();
            if result_path.is_err() {
                panic!("Could not get entry: '{}'", entry_name);
            }           

            if path.file_name().unwrap().to_os_string().into_string().unwrap().starts_with(".") == false {
                if metadata.is_dir() {
                    //If Dir then recursively go through all children            
                    current_directory.directories.push(structs::Directory { name: entry_name.to_owned(), files: vec!(), directories: vec!() });
                    traverse_directories(&quiet_mode, &arguments, &start_root_directory, &entry_name.to_owned(), current_directory, &mut tree_analysis, ym_argument, overwrite_mode, &do_only_this_directory);
                } else if metadata.is_file() && dont_do_anything_to_files==false {            
                    //If File then add it to the current Directory's files vector
                    tree_analysis.total_files += 1;                
                    //let file_name_only = path.file_stem().unwrap().to_os_string().into_string().unwrap();
                    let file_name = path.file_name().unwrap().to_os_string().into_string().unwrap();
                                                    
                    let new_file_object = structs::File {
                        name: file_name,
                        size_in_bytes: metadata.len(),
                        modified_date: metadata.modified().unwrap(),
                        creation_date: metadata.modified().unwrap(),
                        embedded_date: metadata.modified().unwrap(),
                        directory_name: start_directory.to_owned()
                    };

                    if ym_argument != String::new() {                                                            
                        add_file_to_directory_vector_with_name(&mut current_directory, &new_file_object);
                        do_file_action(&quiet_mode, &start_root_directory, &new_file_object, &arguments, &ym_argument, overwrite_mode);
                    } else {
                        if !find_file_duplicate(&quiet_mode, &new_file_object, &current_directory, &mut tree_analysis) {                    
                            add_file_to_directory_vector_with_name(&mut current_directory, &new_file_object);
                            do_file_action(&quiet_mode, &start_root_directory, &new_file_object, &arguments, &ym_argument, overwrite_mode); //<-- Escapes here
                        }
                    }
                }
            }
        }
    } else {        
        panic!("Could not read directory: '{}'", start_directory);
    }
}

fn find_file_duplicate(quiet_mode: &bool, new_file: &structs::File, directory: &structs::Directory, mut tree_analysis: &mut structs::TreeAnalysis) -> bool {
    //check files in current directory
    for existing_file in &directory.files {
        if existing_file.name == new_file.name && existing_file.size_in_bytes == new_file.size_in_bytes {
            tree_analysis.total_duplicate_files += 1;
            let file_size_string = format!("{}MB", round_float(new_file.size_in_bytes as f64 / (1024.0 * 1024.0)));                
            if !quiet_mode {
                println!("{}:{}{}{} -> {}{}{}", file_size_string, existing_file.directory_name, get_platform_splitter(), existing_file.name, new_file.directory_name, get_platform_splitter(), new_file.name);
            }
            tree_analysis.size_to_delete = tree_analysis.size_to_delete + new_file.size_in_bytes;
            return true;
        }
    }

    //Check child directories
    for existing_directory in &directory.directories {       
        for existing_file in &existing_directory.files {
            if existing_file.name == new_file.name && existing_file.size_in_bytes == new_file.size_in_bytes {
                tree_analysis.total_duplicate_files += 1;
                let file_size_string = format!("{}MB", round_float(new_file.size_in_bytes as f64 / (1024.0 * 1024.0)));                
                if !quiet_mode {
                    println!("{}:{}{}{} -> {}{}{}", file_size_string, existing_file.directory_name, get_platform_splitter(), existing_file.name, new_file.directory_name, get_platform_splitter(), new_file.name);
                }
                tree_analysis.size_to_delete = tree_analysis.size_to_delete + new_file.size_in_bytes;
                return true;
            }
        }
        find_file_duplicate(&quiet_mode, &new_file, &existing_directory, &mut tree_analysis);
    }
    false
}

fn get_new_directory_name(start_directory: &str, file_path: &std::path::Path, get_directory_name_from_file: bool, file_modified_date: &std::time::SystemTime, ym_argument: &str) -> String {    
    if get_directory_name_from_file {
        let option_new_file_directory: Option<String> = embedded_data::try_read_file_exif_date(&file_modified_date, &file_path, &ym_argument);
        
        match option_new_file_directory {
            Some(directory) => directory,
            None => String::from(start_directory) 
        };    
    }

    String::from(start_directory)
}

fn round_float(value: f64) -> f64 {
    let mut result = ((value * 100000_f64).round()) / 100000_f64;        
    if result == 0.0 {
        result = 0.00001;
    }
    result
}

fn add_file_to_directory_vector_with_name(directory: &mut structs::Directory, new_file: &structs::File) -> bool {
    let new_file_copy = structs::File {
        name: new_file.name.to_owned(),
        embedded_date: new_file.embedded_date,
        modified_date: new_file.modified_date,
        size_in_bytes: new_file.size_in_bytes,
        directory_name: new_file.directory_name.to_owned(),
        creation_date: new_file.creation_date
    };

    if directory.directories.len()>0 {
        for directory_searched in &mut directory.directories {
            if directory_searched.name == new_file.directory_name {            
                directory_searched.files.push(new_file_copy);
                return true;
            };
        }
    } else {
        directory.files.push(new_file_copy);
    }
    false
}

fn do_file_action (quiet_mode: &bool, start_root_directory: &str, new_file_object: &structs::File, arguments: &Vec<cmd_line_arguments::Argument>, ym_argument: &str, overwrite_mode: &bool) {
    if cmd_line_arguments::argument_exists("-r", &arguments) {
        return;
    } else {
        let move_files = cmd_line_arguments::argument_exists("-m", &arguments);

        let mut action_type = String::from("Found");
        if move_files {
            action_type = String::from("Moving to");
        } else {
            action_type = String::from("Copying to");
        }


        let in_directory_option = &cmd_line_arguments::get_argument_value("-i", &arguments);
        let in_directory = match in_directory_option {
            Some(value) => value.to_owned(),
            None => { panic!("No input root directory argument specified with -i, try -h for help"); },
        };

        let move_directory_start_option = &cmd_line_arguments::get_argument_value("-o", &arguments);
        let move_directory_start = match move_directory_start_option {
            Some(value) => value.to_owned(),
            None => { panic!("No output root directory argument specified with -o , try -h for help"); },
        };
        if move_directory_start != String::new() {
            let from_path = format!("{}{}{}",new_file_object.directory_name, get_platform_splitter(), new_file_object.name);        
            let to_path = get_new_destination_path(&quiet_mode, &action_type, &start_root_directory, &new_file_object.directory_name, &new_file_object.name, &move_directory_start, &new_file_object.modified_date, &ym_argument);        

            let io_result = std::fs::create_dir_all(&to_path.0);
            if io_result.is_err() {
                panic!("Could not create path to save file: {}", to_path.0);
            }

            let mut do_overwrite_of_existing = true;
            if std::path::Path::new(&to_path.1).exists()
            {
                println!("Current file: {}", from_path);
                println!("File already exists at destination path: {}", to_path.1);                
                if *overwrite_mode { 
                    println!("Do you wish to overwrite it? (Y/n): Y");
                    do_overwrite_of_existing = true;
                } else {
                loop {                    
                    println!("Do you wish to overwrite it? (Y/n): ");                    
                    let mut input_from_user: String = String::new();
                    let s = std::io::stdin().read_line(&mut input_from_user);
                    if s.is_ok(){
                        if input_from_user.to_uppercase().trim()=="Y" || input_from_user.trim()=="" {
                            do_overwrite_of_existing = true;
                            break;
                        } else if input_from_user.to_uppercase().trim()=="N" {
                            do_overwrite_of_existing = true;
                            break;
                        }
                    }
                    println!("");
                }
            }
            }
            if do_overwrite_of_existing {
                if move_files {
                    let result = std::fs::rename(&from_path, &to_path.1);
                    if result.is_err() {
                        println!("Could not move file {} to {} \r\n {}", from_path, to_path.1, result.unwrap_err());
                    }
                } else {
                    let result = std::fs::copy(&from_path, &to_path.1);
                    if result.is_err() {
                        println!("Could not copy file {} to {} \r\n {}", from_path, to_path.1, result.unwrap_err());
                    }
                }
            }
        }
    }        
}

fn get_new_destination_path(quiet_mode: &bool, action_type: &str, start_path: &str, current_path: &str, file_name: &str, destination_start_path: &str, file_modified_date: &std::time::SystemTime, ym_argument: &str) -> (String, String) {
    let mut to_chars: Vec<char> = start_path.chars().collect();
    let len_of_start_path = to_chars.len();
    let mut split_tuple: (&str, &str) = current_path.split_at(len_of_start_path);
    
    if String::from(split_tuple.1).starts_with(&get_platform_splitter()) {
        split_tuple.1 = split_tuple.1.split_at(1).1;
    }

    let path = Path::new(destination_start_path);
    let file_name_path = std::path::Path::new(file_name);

    //let current_path = path.join(Path::new(split_tuple.0));
    let current_path = Path::new(&current_path);
    let mut new_path = path.join(Path::new(split_tuple.1)); 
    //println!("current_path: '{}', new_path:'{}'", current_path.display().to_string(), new_path.display().to_string());   
    if ym_argument != String::new() {
        let option_path_ym = embedded_data::try_read_file_exif_date(&file_modified_date, Path::new(&current_path.join(file_name_path).display().to_string()), ym_argument);
        new_path = match option_path_ym {
            Some(ym_path) => Path::new(destination_start_path).join(Path::new(&ym_path)),
            None => new_path
        };
    }
    //let new_path_string: String = get_new_ym_path(&current_path, &file_name_path, &file_modified_date, ym_argument);  

    let new_path_with_file_name = new_path.join(file_name_path);
    if !quiet_mode {
        println!("{} {}", action_type, new_path_with_file_name.as_os_str().to_string_lossy().to_string());
    }    
    (new_path.as_os_str().to_string_lossy().to_string(), new_path_with_file_name.as_os_str().to_string_lossy().to_string())
}

fn get_new_ym_path(current_path: &std::path::Path, file_name_path: &std::path::Path, file_modified_date: &std::time::SystemTime, ym_argument: &str) -> String {
    let option = embedded_data::try_read_file_exif_date(&file_modified_date, Path::new(&current_path.join(file_name_path).display().to_string()), &ym_argument);
    match option {
        Some(new_path) => new_path,
        None => file_name_path.display().to_string()
    }
}

pub fn get_platform_splitter() -> String {
    #[cfg(target_family = "unix")]return String::from("/");
    #[cfg(target_family = "windows")]return String::from("\\");
}