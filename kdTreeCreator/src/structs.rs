//***** Represents a single directory detected by the system
#[derive(Clone)]
pub struct Directory {    
    pub name: String,
    pub files: Vec<File>,
    pub directories: Vec<Directory>,
    //pub parent_directory: Option<Box::<Directory>>
}

//***** Represents a single file detected by the system
#[derive(Clone)]
pub struct File {
    pub size_in_bytes: u64,
    pub name: String,
    pub creation_date: std::time::SystemTime,
    pub modified_date: std::time::SystemTime,
    pub embedded_date: std::time::SystemTime,
    pub directory_name: String
}

//***** Represents a single file detected by the system
#[derive(Clone)]
pub struct TreeAnalysis {
    pub size_to_delete: u64,
    pub total_directories: u64,
    pub total_files: u64,
    pub total_duplicate_files: u64,
}