use std::env;

pub struct Argument{
    pub index: usize,
    pub name: String,
    pub value: String
}

//Get arguments from command line
pub fn get_arguments() -> Result<Vec<Argument>,()> {            
    let args_array: Vec<String> = env::args().collect::<Vec<String>>();
    let arguments: Result<Vec<Argument>, ()> = get_settings(args_array);
    
    arguments
}

//If an argument exists on the command line then add it and its value to Argument Vec to return
fn get_settings(args_array: Vec<String>) -> Result<Vec<Argument>, ()> {    
    let mut arguments_to_return: Vec<Argument> = vec!();

    if args_array.len() == 1 {    
        println!("{}", get_help_screen_text());    
        return Err(())
    }
    
    let mut index_ym_exists: bool = false;
    let mut index_i_exists: bool = false;
    let mut index_o_exists: bool = false;
    let mut index_r_exists: bool = false;
    let mut index_m_exists: bool = false;

    let index_h = get_index_of_arg("-h", &args_array); //Help screen
    let index_i = get_index_of_arg("-i", &args_array); //Input directory path
    let index_o = get_index_of_arg("-o", &args_array); //Output directory path
    let index_m = get_index_of_arg("-m", &args_array); //Move first instance of a file to output directory path: 0=copy, 1=move
    let index_ym = get_index_of_arg("-ym", &args_array); //Move first instance of a file to a new output directory path named according to the oldest year found (create date, jpeg date etc)
    let index_r = get_index_of_arg("-r", &args_array); //Smile screen
    let index_q = get_index_of_arg("-q", &args_array); //Less reporting, quiet mode        
    let index_y = get_index_of_arg("-y", &args_array); //Ask before overwriting a destination file
    let index_d = get_index_of_arg("-d", &args_array); //List of directories to process first
    let index_s = get_index_of_arg("-s", &args_array); //Smile screen    
    
    if index_h.index > 0 {
        println!("{}", get_help_screen_text());
        return Err(());
    } else {
        if index_s.index > 0 {
            println!("{}", get_smile_screen_text());
        };
        if index_i.index > 0 {
            index_i_exists = true;
            arguments_to_return.push(index_i);        
        };
        if index_o.index > 0 {
            index_o_exists = true;
            arguments_to_return.push(index_o);        
        };
        if index_m.index > 0 {
            index_m_exists = true;
            arguments_to_return.push(index_m);        
        };
        if index_ym.index > 0 {
            index_ym_exists = true;
            arguments_to_return.push(index_ym);        
        };
        if index_r.index > 0 {
            index_r_exists = true;
            arguments_to_return.push(index_r);
        };
        if index_y.index > 0 {
            arguments_to_return.push(index_y);
        };
        if index_q.index > 0 {
            arguments_to_return.push(index_q);        
        };
        if index_d.index > 0 {
            arguments_to_return.push(index_d);        
        };
    }
    if index_r_exists {
        if index_m_exists || index_ym_exists {
            println!("If argument '-r' is given then arguments '-m', '-o' and '-ym' may not also be given");
            return Err(());
        }
    } else if !index_o_exists {
        println!("You have not specified a destination directory with argument '-o'");
            return Err(());
    };

    if index_ym_exists {
        if !index_i_exists || !index_o_exists {
            println!("If argument '-ym' is given then arguments '-i' and '-o' must also be given, and optionally '-m' and '-y' too");
            return Err(());
        }
    };

    Ok(arguments_to_return)
}

//Get the index of the argument we want in the command line argments array 
fn get_index_of_arg(arg_id: &str, args_array: &Vec<String>) -> Argument {
    let arg_index = args_array.iter().position(|arg| arg.starts_with(arg_id)).unwrap_or(0);    
    let mut argument_data = Argument {
        index: arg_index,
        name: arg_id.to_owned(),
        value: String::new()
    };

    if arg_index >= 1 {        
        let result = get_arg_value(arg_index, args_array);
        if result.is_ok() {
            argument_data.value=result.unwrap();
        }
    }

    argument_data
}

fn get_arg_value(index: usize, arg_array: &Vec<String>) -> Result<String,()> {
    if arg_array.len() > index + 1 {
        let next_value = arg_array[index + 1].to_owned();
        if next_value.starts_with("-") {
            Ok(String::new())
        } else {            
            Ok(next_value)
        }
    } else {
        Err(())
    }
}

//Get the value of an argument if it was supplied on the command line
pub fn get_argument_value(argument_name: &str, arguments: &Vec<Argument>) -> Option<String> {
    for argument in arguments {
        if argument.name == argument_name {
            return Some(argument.value.to_owned());
        }
    };
    None
}

//Get the value of an argument if it was supplied on the command line
pub fn argument_exists(argument_name: &str, arguments: &Vec<Argument>) -> bool {
    for argument in arguments {
        if argument.name == argument_name {
            return true
        }
    };
    
    false
}


fn get_help_screen_text() -> String {
    let mut help_screen_text = format!("kdTreeCreator version: 0.5.2");
    help_screen_text = help_screen_text + "\n\nGet rid of duplicate files. Duplicate files are defined by having the same NAME AND same byte SIZE";
    help_screen_text = help_screen_text + "\n\nNOTE: Directories and files that start with a . are ignored, so a directory named \"/home/dean/.data\" will not be traversed";
    help_screen_text = help_screen_text + "\n\nWritten by Dean Komen 2020";
    help_screen_text = help_screen_text + "\nSource code available at: https://www.gitlab.com/dkomen/kdTreeCreator";
    help_screen_text = help_screen_text + "\n\nCommand-line arguments:";
    help_screen_text = help_screen_text + "\n    -h    : 'Help': Display this help screen";
    help_screen_text = help_screen_text + "\n    -i    : 'Input directory': The full path to the source directory to scan if argument -o is not also supplied";
    help_screen_text = help_screen_text + "\n            then only a report of space savings that can be gained is shown";
    help_screen_text = help_screen_text + "\n    -o    : 'Output directory': The full destination path to copy or move the non-duplicate files to";
    help_screen_text = help_screen_text + "\n    -r    : 'Report mode': Dont move or copy anything, just report on findings";
    help_screen_text = help_screen_text + "\n    -m    : 'Move the files':Move the clean files from the source to the destination directory";
    help_screen_text = help_screen_text + "\n            Default if not supplied is to copy the files";
    //help_screen_text = help_screen_text + "\n    -y    : A character set to use";
    help_screen_text = help_screen_text + "\n    -q    : 'Quiet mode': Don't display a list of all the duplicate files found, just show the final findings report";
    help_screen_text = help_screen_text + "\n            Default if not supplied is to show the duplicates files found";    
    help_screen_text = help_screen_text + "\n    -ym    : 'Output folder structure': Copy and sort photos by year or year and month into a new directory tree";
    help_screen_text = help_screen_text + "\n            Moves or copies (-m) all files in input directory (-i) to an output directory (-o) with sub-directories where";
    help_screen_text = help_screen_text + "\n            files are ordered by year named directory";
    help_screen_text = help_screen_text + "\n            0 = Create a new tree based only on years, 1 = create a new tree based on year and month";
    help_screen_text = help_screen_text + "\n            You must also supply the -i and -o arguments";
    help_screen_text = help_screen_text + "\n            The date used to create the year/month destination path is the files Modified date or in the case of jpg/jpeg and some tiff";
    help_screen_text = help_screen_text + "\n            files the file is opened and a creation date tag is searched for in it instead";
    help_screen_text = help_screen_text + "\n    -y    : 'Answer yes': Automatically ignore any file overwrite warnings. If not supplied then a Yes/no question will be asked";
    help_screen_text = help_screen_text + "\n            before overwriting any files in the destination directory. Just pressing <enter> when asked to overwrite is the same as";
    help_screen_text = help_screen_text + "\n            entering Y.";
    help_screen_text = help_screen_text + "\n    -d    : 'Important directories': Start by processing these directories first (in the order supplied) and if duplicates are found anywhere prefer";
    help_screen_text = help_screen_text + "\n             to keep the files in these directories first. The directory list must be supplied as a comma seperated list inside a double quote \"KeepPath1,Keep Path2\"";
    //help_screen_text = help_screen_text + "\n    -s    : To see a lovely smile";
    help_screen_text = help_screen_text + "\n\nExamples";
    help_screen_text = help_screen_text + "\n\nThese examples show the use of Linux path names, on Windows use Windows style path names, like: c:\\MyPhotos";
    help_screen_text = help_screen_text + "\n    1. Analyse a directory for number of duplicate files and report all duplicates as they are found found";    
    help_screen_text = help_screen_text + "\n       kdTreeCreator -i /home/dean/Pictures -r";
    help_screen_text = help_screen_text + "\n    2. Analyse a directory for number of duplicate files but only report the final statistics";    
    help_screen_text = help_screen_text + "\n       kdTreeCreator -i -q /home/dean/Pictures -r";
    help_screen_text = help_screen_text + "\n    3. Generate a new output directory and COPY all the duplicate cleaned files to it. Leaves the source directory files untouched.";
    help_screen_text = help_screen_text + "\n       kdTreeCreator -i /home/dean/Pictures -o /home/dean/PicturesClean";    
    help_screen_text = help_screen_text + "\n    4. Generate a new output directory and COPY all the duplicate cleaned files to it. Process a supplied ordered list of given directories first.";
    help_screen_text = help_screen_text + "\n       Leaves the source directory files untouched.";
    help_screen_text = help_screen_text + "\n       kdTreeCreator -i /home/dean/Pictures -o /home/dean/PicturesClean -d \"/home/dean/Pictures/ScanMeFirst,/home/dean/Pictures/ScanMeSecond\"";    
    help_screen_text = help_screen_text + "\n    5. Generate a new output directory and MOVE all the duplicate cleaned file to it, leaving the rest (duplicates) in the source folder.";
    help_screen_text = help_screen_text + "\n       kdTreeCreator -i /home/dean/Pictures -o /home/dean/PicturesClean -m";
    help_screen_text = help_screen_text + "\n    6. Generate a new output directory and MOVE all the files into a new directory (-o) where sub-directories of year and month are created";
    help_screen_text = help_screen_text + "\n       to put the files found into. (-y) Indicates to no ask to overrwrite same named files in the destination path";
    help_screen_text = help_screen_text + "\n       kdTreeCreator -i /home/dean/MyPhotos -o /home/dean/PhotosByYearMonth -m -ym 1 -y";
    help_screen_text = help_screen_text + "\n       This function does not remove duplicates as such but does overrwrite target destination files with duplicates in the source, thus having";
    help_screen_text = help_screen_text + "\n       no duplicates in the same target folder. So perhaps do this after first getting rid of duplicates, see examples 3 and 4 above";
    // help_screen_text = help_screen_text + "\n    6. Bonus argument, try it... it's safe";
    // help_screen_text = help_screen_text + "\n       kdTreeCreator -s";    
    help_screen_text = help_screen_text + "\n\n\n";
    help_screen_text
}

fn get_smile_screen_text() -> String {
    let mut help_screen_text = format!("\n");
    help_screen_text = help_screen_text + "\n    (ʘ‿ʘ)";
    help_screen_text = help_screen_text + "\n";
    help_screen_text = help_screen_text + "\n\n\n";
    help_screen_text
}