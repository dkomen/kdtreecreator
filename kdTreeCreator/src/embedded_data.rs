use chrono::{ NaiveDateTime, Datelike, DateTime, Utc };

pub fn try_read_file_exif_date(file_modified_date: &std::time::SystemTime, file_path: &std::path::Path, ym_argument: &str) -> Option<String> {
    //println!("Searching for Exif in file_path: {}", file_path.display().to_string());
    let resut_file = std::fs::File::open(file_path);
    if resut_file.is_ok() {
        let file = resut_file.unwrap();
        let mut bufreader = std::io::BufReader::new(file);
        let exifreader = exif::Reader::new();
        let option_exif = exifreader.read_from_container(&mut bufreader);
        if option_exif.is_ok() {
            //println!("File has exif");
            let exif = option_exif.unwrap();
            for field in exif.fields() {
                //println!("{}{}{}", field.tag, field.ifd_num, field.display_value().with_unit(&exif));
                if field.tag.to_string().starts_with("DateTimeOriginal") 
                || field.tag.to_string().starts_with("Date and Time (original)")
                || field.tag.to_string().starts_with("Date and Time Original") {
                    let result_date_time = NaiveDateTime::parse_from_str(&field.display_value().with_unit(&exif).to_string(), "%Y-%m-%d %H:%M:%S");
                    if result_date_time.is_ok() {
                        let date_time = result_date_time.unwrap();                        
                        if ym_argument == String::from("0") {
                            return Some(format!("{}", date_time.year()));
                        } else {
                            return Some(format!("{}{}{}", date_time.year(), super::get_platform_splitter(), format!("{}-{}",date_time.year(), date_time.month())));
                        }
                    }
                                    
                };                
            };            
            for field in exif.fields() {            
                if field.tag.to_string().starts_with("DateTime")
                || field.tag.to_string().starts_with("Date Time")
                || field.tag.to_string().starts_with("Date and Time") {
                    let result_date_time = NaiveDateTime::parse_from_str(&field.display_value().with_unit(&exif).to_string(), "%Y-%m-%d %H:%M:%S");
                    if result_date_time.is_ok() {
                        let date_time = result_date_time.unwrap();                        
                        if ym_argument == String::from("0") {
                            return Some(format!("{}", date_time.year()));
                        } else {
                            return Some(format!("{}{}{}", date_time.year(), super::get_platform_splitter(), format!("{}-{}",date_time.year(), date_time.month())));
                        }
                    }
                                    
                };                
            };            
            
        };        

        let system_time: std::time::SystemTime = *file_modified_date;
        let datetime: DateTime<Utc> = system_time.into();
        if ym_argument == String::from("0") {
            return Some(format!("{}", datetime.year()));
        } else {
            //return Some(format!("{}{}{}", datetime.year(), super::get_platform_splitter(), datetime.month()));
            return Some(format!("{}{}{}", datetime.year(), super::get_platform_splitter(), format!("{}-{}",datetime.year(), datetime.month())));
        }
        //return Some(format!("{}", datetime.format("%Y-%m-%d %H:%M:%S")));
    } else {
        panic!("Could not open file '{}' to get Exif info", file_path.display().to_string());
    }
}